package com.brave.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("corp")
public class CorporationBaseInfo {

	String compid;

	Secret secret = new Secret();

	public String getCompid() {
		return compid;
	}

	public void setCompid(String compid) {
		this.compid = compid;
	}

	public Secret getSecret() {
		return secret;
	}

	public static class Secret {
		String contact;
		String customer;

		public String getContact() {
			return contact;
		}

		public void setContact(String contact) {
			this.contact = contact;
		}

		public String getCustomer() {
			return customer;
		}

		public void setCustomer(String customer) {
			this.customer = customer;
		}
	}

}
