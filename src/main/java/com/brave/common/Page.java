package com.brave.common;

import java.io.Serializable;

import lombok.Data;

/**
 * @author junzhang
 */
@Data
public abstract class Page implements Serializable {

	private String merchantId;

	private int pageNum = 1;

	private int pageSize = 10;

	private boolean needTotalCount = true;

	public int getOffset() {
		return pageNum > 0 ? (pageNum - 1) * pageSize : 0;
	}
}
