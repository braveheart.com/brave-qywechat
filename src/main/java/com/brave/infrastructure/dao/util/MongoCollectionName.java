package com.brave.infrastructure.dao.util;

public class MongoCollectionName {
	public static final String TOKEN = "token";
	public static final String DEPARTMENT = "department";
	public static final String EMPLOYEE = "employee";
}
