package com.brave.infrastructure.dao;

import com.brave.infrastructure.dao.po.EmployeePO;
import com.brave.infrastructure.dao.util.MongoCollectionName;

import org.springframework.stereotype.Repository;

@Repository
public class EmployeeDAO extends BaseDAO {

	/**
	 *
	 * @param employeePO
	 */
	public void addEmployee(EmployeePO employeePO) {
		mongoTemplate.save(employeePO, MongoCollectionName.EMPLOYEE);
	}
}
