package com.brave.infrastructure.dao;

import java.util.List;

import com.brave.infrastructure.dao.po.TokenPO;
import com.brave.infrastructure.dao.util.MongoCollectionName;
import com.brave.infrastructure.dao.util.TokenStatus;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class TokenDAO extends BaseDAO {

	public void saveToken(TokenPO tokenPO) {
		mongoTemplate.save(tokenPO, MongoCollectionName.TOKEN);
	}

	public List<TokenPO> getToken(String tokenType) {
		Query query = new Query(Criteria.where("type").is(tokenType).and("status").is(TokenStatus.AVALI));
		List<TokenPO> tokenPOs = mongoTemplate.find(query, TokenPO.class, MongoCollectionName.TOKEN);
		return tokenPOs;
	}

	public void expireToken(String token) {
		Query query = new Query(Criteria.where("accessToken").is(token));
		Update update = new Update();
		update.set("status",TokenStatus.EXPIRE);
		mongoTemplate.updateFirst(query, update,MongoCollectionName.TOKEN);
	}
}
