package com.brave.client.dto;

import java.util.List;

import lombok.Data;

@Data
public class EmployeeResultDTO extends WXBaseDTO {
	List<EmployeeDTO> userlist;
}
