package com.brave.client.dto;

import lombok.Data;

@Data
public class EmployeeDTO {
	String userid;
	String name;
	Integer[] department;
	String open_userid;
}
