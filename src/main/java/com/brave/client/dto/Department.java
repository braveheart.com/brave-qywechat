package com.brave.client.dto;

import lombok.Data;

@Data
public class Department {
	Integer id;
	String name;
	String name_en;
	Integer parentid;
	Integer order;
}
