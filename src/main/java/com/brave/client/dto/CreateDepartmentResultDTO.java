package com.brave.client.dto;

import lombok.Data;

@Data
public class CreateDepartmentResultDTO extends WXBaseDTO {
	Integer id;
}
