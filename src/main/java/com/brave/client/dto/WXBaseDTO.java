package com.brave.client.dto;

import lombok.Data;

@Data
public class WXBaseDTO {
	Integer errcode;
	String errmsg;
}
