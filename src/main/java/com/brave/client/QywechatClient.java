package com.brave.client;

import com.brave.client.dto.AccessTokenDTO;
import com.brave.client.dto.CreateDepartmentResultDTO;
import com.brave.client.dto.Department;
import com.brave.client.dto.DepartmentsDTO;
import com.brave.client.dto.EmployeeResultDTO;
import com.brave.client.dto.WXBaseDTO;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "qywechat",url = "${wechat.url}")
public interface QywechatClient {

	/**
	 * 获取联系人的token
	 * @param corpId
	 * @param corpsecret
	 * @return
	 */
	@GetMapping("/cgi-bin/gettoken")
	AccessTokenDTO getContactToken(@RequestParam("corpid") String corpId,@RequestParam("corpsecret") String corpsecret);

	/**
	 * 获取所有部门列表
	 * @param contactSecret
	 * @return
	 */
	@GetMapping("/cgi-bin/department/list")
	DepartmentsDTO getDepartments(@RequestParam("access_token") String contactSecret);


	/**
	 * 更新部门
	 * @param contactSecret
	 * @param department
	 * @return
	 */
	@PostMapping("/cgi-bin/department/update")
	WXBaseDTO updateDepartment(@RequestParam("access_token") String contactSecret,@RequestBody Department department);

	/**
	 * 创建部门
	 * @param contactSecret
	 * @param department
	 * @return
	 */
	@PostMapping("/cgi-bin/department/create")
	CreateDepartmentResultDTO createDepartment(@RequestParam("access_token") String contactSecret,@RequestBody Department department);

	/**
	 * 删除部门
	 * @param contactSecret
	 * @param id
	 * @return
	 */
	@GetMapping("/cgi-bin/department/delete")
	WXBaseDTO deleteDepartment(@RequestParam("access_token") String contactSecret,@RequestParam("id") Integer id);

	/**
	 * 获取部门成员
	 * @param contactSecret
	 * @param departmentId
	 * @param fetchChild
	 * @return
	 */
	@GetMapping("/cgi-bin/user/simplelist")
	EmployeeResultDTO getEmployees(@RequestParam("access_token") String contactSecret,@RequestParam("department_id") String departmentId,@RequestParam("fetch_child") String fetchChild );

}
