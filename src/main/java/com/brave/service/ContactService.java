package com.brave.service;

import java.util.List;

import com.brave.client.QywechatClient;
import com.brave.client.dto.Department;
import com.brave.client.dto.DepartmentsDTO;
import com.brave.infrastructure.dao.DepartmentDAO;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ContactService {
	@Autowired
	QywechatClient qywechatClient;
	@Autowired
	DepartmentDAO departmentDAO;

	/**
	 * 查询所有的部门列表
	 * @param contactSecret
	 * @return
	 */
	public List<Department> getDepartments(String contactSecret) {
		DepartmentsDTO departmentsDTO = qywechatClient.getDepartments(contactSecret);
		if(null != departmentsDTO) {
			departmentsDTO.getDepartment().stream().forEach(department -> {
				log.info("department is {}",department);
				departmentDAO.addDepartment(department);
			});

			return departmentsDTO.getDepartment();
		}
		return null;

	}


}
