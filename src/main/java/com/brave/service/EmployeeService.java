package com.brave.service;

import java.util.List;

import com.brave.client.QywechatClient;
import com.brave.client.dto.EmployeeDTO;
import com.brave.client.dto.EmployeeResultDTO;
import com.brave.infrastructure.dao.EmployeeDAO;
import com.brave.infrastructure.dao.po.EmployeePO;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EmployeeService {

	@Autowired
	EmployeeDAO employeeDAO;

	@Autowired
	QywechatClient qywechatClient;

	/**
	 * 获取所有部门员工
	 * @return
	 */
	public List<EmployeeDTO> getEmployees(String contactToken,String departmentId) {
		//fetchchild:是否递归获取子部门下面的成员：1-递归获取，0-只获取本部门
		EmployeeResultDTO resultDTO  = qywechatClient.getEmployees(contactToken,departmentId,"0");
		if(null != resultDTO && resultDTO.getErrcode() == 0) {
			List<EmployeeDTO> userlist = resultDTO.getUserlist();
			if(null != userlist && userlist.size()>0 ) {
				userlist.stream().forEach(employeeDTO -> {
					EmployeePO employeePO = new EmployeePO();
					employeePO.setDepartment(employeeDTO.getDepartment());
					employeePO.setName(employeeDTO.getName());
					employeePO.setOpen_userid(employeeDTO.getOpen_userid());
					employeePO.setUserid(employeeDTO.getUserid());
					employeeDAO.addEmployee(employeePO);
				});
				return userlist;
			}
		}
		return null;
	}
}
