package com.brave.controller;

import java.util.List;

import com.brave.client.dto.Department;
import com.brave.common.MultiResponse;
import com.brave.common.Response;
import com.brave.service.ContactService;
import com.brave.service.TokenService;
import com.brave.service.util.WXTokenType;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/contacts/v1")
public class ContactsController {

	@Autowired
	ContactService contactService;

	@Autowired
	TokenService tokenService;


	@GetMapping("/departments")
	MultiResponse<Department> getDepartments() {
		List<Department> results = contactService.getDepartments(tokenService.getTokenLocal(WXTokenType.CONTACT));
		if(null != results) {
			return MultiResponse.ofWithoutTotal(results);
		}else {
			return MultiResponse.buildSuccess();
		}
	}
}
