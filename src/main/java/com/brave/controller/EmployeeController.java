package com.brave.controller;

import java.util.ArrayList;
import java.util.List;

import com.brave.client.dto.Department;
import com.brave.client.dto.EmployeeDTO;
import com.brave.common.MultiResponse;
import com.brave.service.ContactService;
import com.brave.service.EmployeeService;
import com.brave.service.TokenService;
import com.brave.service.util.WXTokenType;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/employees/v1")
public class EmployeeController {
	@Autowired
	EmployeeService employeeService;

	@Autowired
	ContactService contactService;

	@Autowired
	TokenService tokenService;


	@GetMapping
	public MultiResponse<EmployeeDTO> getEmployees() {
		List<EmployeeDTO> result = new ArrayList<>();
		String contactToken = tokenService.getTokenLocal(WXTokenType.CONTACT);
		List<Department> departments = contactService.getDepartments(contactToken);
		if(null != departments && departments.size()>0) {
			departments.stream().forEach(department -> {
				List<EmployeeDTO> employees = employeeService.getEmployees(contactToken,String.valueOf(department.getId()));
				if(null != employees) {
					result.addAll(employees);
				}
			});
		}
		return MultiResponse.ofWithoutTotal(result);
	}

}
